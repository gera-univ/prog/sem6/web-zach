package org.spiralarms.hellotomcat;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "DeleteServlet", value = "/delete")
public class DeleteServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (deleteUser(req, resp)) {
            RequestDispatcher dispLogout = req.getRequestDispatcher("/logout");
            dispLogout.forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (deleteUser(req, resp)) {
            RequestDispatcher dispLogout = req.getRequestDispatcher("/logout");
            dispLogout.forward(req, resp);
        }
    }

    public boolean deleteUser(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        HttpSession session = req.getSession();
        String username = (String) session.getAttribute("name");
        if (username != null) {
            try {
                UserDAO.deleteUserByName(username);
            } catch (SQLException e) {
                resp.getOutputStream().println("SQL error");
                return false;
            }
        }
        return true;
    }
}
