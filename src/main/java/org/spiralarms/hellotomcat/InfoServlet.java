package org.spiralarms.hellotomcat;

import javax.persistence.NoResultException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "infoServlet", value = "/info")
public class InfoServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("info get");
        if (!handleRequest(req, resp)) return;
        req.getRequestDispatcher("/info.jsp").forward(req, resp);
        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("info post");
        if (!handleRequest(req, resp)) return;
        req.getRequestDispatcher("/info.jsp").forward(req, resp);
        super.doPost(req, resp);
    }

    public boolean handleRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispForm = req.getRequestDispatcher("/form.jsp");

        HttpSession session = req.getSession();
        String username = (String) session.getAttribute("name");
        String age = (String) session.getAttribute("age");

        if (username == null || age == null) {
            dispForm.forward(req, resp);
            return false;
        }

        try {
            int iage = Integer.parseInt(age);

            if (iage < 0)
                throw new IllegalArgumentException();
        } catch (NumberFormatException e) {
            req.setAttribute("error", "Age must be an integer");
            dispForm.forward(req, resp);
            return false;
        } catch (IllegalArgumentException e) {
            req.setAttribute("error", "Age must be positive");
            dispForm.forward(req, resp);
            return false;
        }

        try {
            try {
                User u = UserDAO.getUserByName(username);
                session.setAttribute("name", u.getName());
                session.setAttribute("age", Integer.toString(u.getAge()));
            } catch (NoResultException e) {
                UserDAO.insertUser((String) session.getAttribute("name"), Integer.parseInt((String) session.getAttribute("age")));
            }

            return true;
        } catch (SQLException e) {
            req.setAttribute("error", "SQL exception");
            dispForm.forward(req, resp);
            return false;
        }
    }
}
