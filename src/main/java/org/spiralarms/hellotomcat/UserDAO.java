package org.spiralarms.hellotomcat;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.transaction.Transactional;
import java.sql.SQLException;
import java.util.ArrayList;

public class UserDAO {
    private static final String persistenceName = "user-login";
    private static final EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory(persistenceName);
    private static EntityManager entityManager = entityManagerFactory.createEntityManager();

    public static User getUserByName(String name) throws SQLException {
        return entityManager
                .createNamedQuery("User.getByName", User.class)
                .setParameter("name", name)
                .getSingleResult();
    }

    public static ArrayList<User> getAllUsers() throws SQLException {
        return new ArrayList<>(entityManager
                .createNamedQuery("User.getAll", User.class)
                .getResultList());
    }

    public static void insertUser(String name, int age) throws SQLException {
        EntityTransaction updateTransaction = entityManager
                .getTransaction();
        updateTransaction.begin();
        ArrayList<User> users = getAllUsers();
        int new_id = users.get(0).getId();
        for (User user : users) {
            if (user.getId() > new_id)
                new_id = user.getId();
            if (user.getName().equals(name))
            {
                int count = entityManager.createNamedQuery("User.update")
                        .setParameter("id", user.getId())
                        .setParameter("name", user.getName())
                        .setParameter("age", user.getAge())
                        .executeUpdate();
                updateTransaction.commit();
                return;
            }
        }
        ++new_id;
        entityManager.createNativeQuery("INSERT INTO users (id, name, age) VALUES (?,?,?)")
                .setParameter(1, new_id)
                .setParameter(2, name)
                .setParameter(3, age)
                .executeUpdate();
        updateTransaction.commit();
    }

    public static void deleteUserByName(String name) throws SQLException {
        EntityTransaction updateTransaction = entityManager
                .getTransaction();
        updateTransaction.begin();
        int count = entityManager.createNamedQuery("User.deleteByName")
                .setParameter("name", name)
                .executeUpdate();
        updateTransaction.commit();
    }
}
