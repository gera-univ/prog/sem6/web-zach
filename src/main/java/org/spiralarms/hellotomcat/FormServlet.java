package org.spiralarms.hellotomcat;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "formServlet", value = "/")
public class FormServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("form get");
        if (handleRequest(req, resp)) return;

        req.getRequestDispatcher("/form.jsp").forward(req, resp);
        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("form post");
        if (handleRequest(req, resp)) return;

        String username = req.getParameter("name");
        String age = req.getParameter("age");

        HttpSession session = req.getSession();
        session.setAttribute("name", username);
        session.setAttribute("age", age);

        req.getRequestDispatcher("/").forward(req, resp);
        super.doPost(req, resp);
    }

    private boolean handleRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        if (req.getAttribute("error") != null) {
            RequestDispatcher dispForm = req.getRequestDispatcher("/form.jsp");
            dispForm.forward(req, resp);
            // form removes error, name, age
            return true;
        }
        if (session.getAttribute("name") != null && session.getAttribute("age") != null) {
            RequestDispatcher dispInfo = req.getRequestDispatcher("/info");
            dispInfo.forward(req, resp);
            return true;
        }

        return false;
    }
}
