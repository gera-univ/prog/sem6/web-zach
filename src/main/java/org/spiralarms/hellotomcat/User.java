package org.spiralarms.hellotomcat;

import javax.persistence.*;

@Entity
@Table(name = "users")
@NamedQueries({
        @NamedQuery(name = "User.getByName", query = "SELECT u FROM User u WHERE name = :name"),
        @NamedQuery(name = "User.getAll", query = "SELECT u FROM User u"),
        @NamedQuery(name = "User.update", query = "UPDATE User u SET name = :name, age = :age WHERE id = :id"),
        @NamedQuery(name = "User.deleteByName", query = "DELETE from User u WHERE name = :name"),
})
public class User {
    @Id
    private int id;
    @Column(name = "name")
    private String name;
    @Column(name = "age")
    private int age;

    public User() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
