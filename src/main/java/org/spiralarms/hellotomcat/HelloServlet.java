package org.spiralarms.hellotomcat;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(name = "helloServlet", value = "/hello-servlet")
public class HelloServlet extends HttpServlet {
    private String message;

    public void init() {
        message = "Hello World!";
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        // Hello
        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        out.println("<h1>" + message + "</h1>");
        out.println("</body></html>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("name");
        String age = req.getParameter("age");

        if (username != null) {
            req.getSession().setAttribute("username", username);
            req.getSession().setAttribute("age", age);
            resp.sendRedirect("form.jsp");
        }
        else {
            req.setAttribute("error", "Unknown user, please try again");
            req.getRequestDispatcher("/login.jsp").forward(req, resp);
        }
    }

    public void destroy() {
    }
}