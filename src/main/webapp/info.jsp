<%@ page import="org.spiralarms.hellotomcat.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<% String name = (String) session.getAttribute("name"); %>
<% String age = (String) session.getAttribute("age"); %>
<head>
    <link rel="StyleSheet" href="<%=request.getContextPath()%>/css/style.css" type="text/css">
</head>
<body>
<div class="content">
    <title>Hello, <%= name %>
    </title>

    Hi <strong><%= name %></strong>. Your age is <%= age %>.

    <br>
    <a href="logout" class="blue">log out</a>
    <br>
    <a href="delete" class="red">delete me from the database</a>
</div>
</body>
</html>
