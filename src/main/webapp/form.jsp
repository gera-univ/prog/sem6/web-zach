<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import="java.io.*,java.util.*" %>
<%@ page import="org.hibernate.Session" %>
<!DOCTYPE html>
<html>
<head>
    <link rel="StyleSheet" href="<%=request.getContextPath()%>/css/style.css" type="text/css">
    <title>Log In form</title>
</head>
<body>
<div class="content">
    </h1>
    <br/>
    <% String error = (String) request.getAttribute("error"); %>
    <% request.removeAttribute("error"); %>
    <%
        session.removeAttribute("name");
        session.removeAttribute("age");
    %>
    <font color="red"><%=error == null ? "" : error%>
    </font>
    <% String name = request.getParameter("name"); %>
    <% String age = request.getParameter("age"); %>
    <form name="add" class="form-container" method="post">
        <input type="text" placeholder="Name" name="name" value="<%= name == null ? "" : name %>" required>
        <br>
        <input type="text" placeholder="Age" name="age" value="<%= age == null ? "" : age %>" required>
        <br>
        <button type="submit" class="btn">Login</button>
    </form>
</div>

</body>
</html>