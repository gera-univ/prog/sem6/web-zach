PostgreSQL is used.

```
create-db user-login
psql user-login
>create table users(id int primary key, name text, age integer);
```

Add `src/main/resources/META-INF/persistence.xml` with the following contents (replace user, password, url).


```xml
<persistence xmlns="http://xmlns.jcp.org/xml/ns/persistence"
             xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
             xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/persistence
http://xmlns.jcp.org/xml/ns/persistence/persistence_2.."
             version="2.1">

    <persistence-unit name="user-login" transaction-type="RESOURCE_LOCAL">
        <properties>
            <property name="hibernate.connection.driver_class" value="org.postgresql.Driver"/>
            <property name="hibernate.connection.password" value="==password=="/>
            <property name="hibernate.connection.url" value="jdbc:postgresql://localhost/user-login"/>
            <property name="hibernate.connection.username" value="==user=="/>
            <property name="hibernate.hbm2ddl.auto" value="update"/>
            <property name="hibernate.dialect" value="org.hibernate.dialect.PostgreSQL82Dialect"/>
        </properties>
    </persistence-unit>

</persistence>
```

Use Intelij Idea Ultimate and tomcat 8 build from [tomcat website](https://tomcat.apache.org/download-80.cgi).
